let productAvailability = [
    {
      productName: 'Choo Choo Chocolate',
      availability: false
    },
    {
      productName: 'Icy mint',
      availability: false
    },
    {
      productName: 'Cake Batter',
      availability: false
    },
    {
      productName: 'Bubblegum',
      availability: true
    }
];
let requiredProduct = productAvailability.find(product => product.productName === 'Bubblegum');
// console.log(requiredProduct);

let str = 'h a s       nothing';

function adjustStr(str) {
    return str.split(' ').filter(value => {
        if (value.length > 0) {
            console.log(value.length);
            return true;
        }
    }).join(' ');
}

let result = adjustStr(str);
console.log('✅',result);
