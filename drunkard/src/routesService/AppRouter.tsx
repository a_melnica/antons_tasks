import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import {RouteNames, routes} from "./index";

export const AppRouter = () => {

    return (
            <Switch>
                {routes.map(route =>
                    <Route path={route.path}
                           exact={route.exact}
                           component={route.component}
                           key={route.path}// important
                    />
                )}
                <Redirect to={RouteNames.LOGIN}/>
            </Switch>

    );
}