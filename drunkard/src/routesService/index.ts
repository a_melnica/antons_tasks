import React from "react";

import AdminRoom from '../components/adminRoom/AdminRoom';
import Drunkard from '../components/drunkard/Drunkard';
import Login from "../components/login/Login";
import Main from "../components/main/Main";
import PersonalRoom from '../components/personalRoom/PersonalRoom';
import TopPlaces from '../components/topPlaces/TopPlaces';


export interface IRoute {
    path: string;
    component: React.ComponentType;
    exact?: boolean;
}

export enum RouteNames {
    ADMIN_ROOM = '/adminRoom',
    DRUNKARD = '/drunkard',
    LOGIN = '/login',
    MAIN = '/main',
    PERSONAL_ROOM = '/personalRoom',
    TOP_PLACES = '/topPlaces'
}

export const routes: IRoute[] = [
    { path: RouteNames.ADMIN_ROOM, exact: true, component: AdminRoom },
    { path: RouteNames.DRUNKARD, exact: true, component: Drunkard },
    { path: RouteNames.LOGIN, exact: true, component: Login },
    { path: RouteNames.MAIN, exact: true, component: Main },
    { path: RouteNames.PERSONAL_ROOM, exact: true, component: PersonalRoom },
    { path: RouteNames.TOP_PLACES, exact: true, component: TopPlaces }
];