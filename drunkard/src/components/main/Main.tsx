import React, {FC} from 'react';

import './Main.css'

const Main: FC = () => {
    return (
        <div>
            <main>
                <div className="header">
                    <div>Lorem ipsum dolor.</div>
                    <h1>Lorem ipsum dolor. <br/>Lorem ipsum dolor sit.</h1>
                </div>
                <div className="content">
                    <img
                        src="https://res.cloudinary.com/jlengstorf/image/upload/v1585239150/lwj-css-grid/restaurant.jpg"
                        alt="a restaurant interior"
                    />
                    <div className="content-inner">
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, eveniet!</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aspernatur ea numquam omnis,
                            possimus quae qui repellendus similique suscipit vel.
                        </p>
                    </div>
                </div>

                <div className="panel-intro">
                    <h2>Lorem ipsum dolor sit amet, consectetur.</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam delectus ea minus placeat unde vitae.
                    </p>
                </div>

                <div className="panel">
                    <img
                        src="https://res.cloudinary.com/jlengstorf/image/upload/v1585239150/lwj-css-grid/soup.jpg"
                        alt="a bowl of soup"
                    />
                    <img
                        src="https://res.cloudinary.com/jlengstorf/image/upload/v1585239150/lwj-css-grid/plate.jpg"
                        alt="a plate on a table"
                    />
                    <img
                        src="https://res.cloudinary.com/jlengstorf/image/upload/v1585239150/lwj-css-grid/food.jpg"
                        alt="a cutting board covered in food"
                    />
                    <img
                        src="https://res.cloudinary.com/jlengstorf/image/upload/v1585239150/lwj-css-grid/egg.jpg"
                        alt="an egg dish that looks delicious"
                    />
                </div>
            </main>

            <footer>
                <div>
                    <h3>Some stuff.</h3>
                    <ul>
                        <li><a href="">something</a></li>
                    </ul>
                </div>

                <div>
                    <h3>Some stuff</h3>
                    <ul>
                        <li><a href="">something</a></li>
                        <li><a href="">something</a></li>
                        <li><a href="">something</a></li>
                    </ul>
                </div>
            </footer>
        </div>
    );
};

export default Main;