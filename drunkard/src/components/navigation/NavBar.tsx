import React, {FC} from 'react';
import {useHistory} from "react-router-dom";

import './NavBar.css'
import {RouteNames} from "../../routesService";

const Navbar: FC = () => {

    const history = useHistory();

    return (
        <div className="nav-wrapper">
            <ul>
                <li onClick={() => history.push(RouteNames.MAIN)}>main</li>
                <li onClick={() => history.push(RouteNames.ADMIN_ROOM)}>admin room</li>
                <li onClick={() => history.push(RouteNames.DRUNKARD)}>drunkard</li>
                <li onClick={() => history.push(RouteNames.PERSONAL_ROOM)}>personal room</li>
                <li onClick={() => history.push(RouteNames.TOP_PLACES)}>top places</li>
                <li onClick={() => history.push(RouteNames.LOGIN)}>login</li>
            </ul>
        </div>
    );
}

export default Navbar;